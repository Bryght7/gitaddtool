$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
     
    var clipboard = new Clipboard('.btn-copy');
    clipboard.on('success', function (e) {
        e.clearSelection();
        if (e.text !== '')
            $('.btn-copy').tooltip('toggle');
    });

    $('.btn-read').click(function () {
        var gitInput = $('.paste-area').val();
        if (gitInput === '') {
            return;
        }

        $('.processed').empty();

        var linesWithFiles = gitInput.match(/^[^\n]\s{4,}(.+)/mg);
        var modified = [];
        var untracked = [];

        if (linesWithFiles === null) {
            return;
        }

        linesWithFiles.forEach(function (f) {
            f = f.replace(/\s+/g, ''); // remove all long spaces
            if (f.startsWith('modified:')) {
                modified.push(f.replace('modified:', ''));
            } else {
                untracked.push(f);
            }
        });

        if (modified.length !== 0) {
            $('.processed').append('<h4 class="file-check-header">Modified</h4>');
            modified.forEach(function (f) {
                $('.processed').append('<input class="file-checkbox" type="checkbox" name="file" value="' + f + '"> ' + f + '<br>');
            });
        }
        if (untracked.length !== 0) {
            $('.processed').append('<h4 class="file-check-header">Untracked</h4>');
            untracked.forEach(function (f) {
                $('.processed').append('<input class="file-checkbox" type="checkbox" name="file" value="' + f + '"> ' + f + '<br>');
            });
        }
    });

    $('.processed').on('change', 'input:checkbox[name=file]', function () {
        var result = '';
        $("input:checkbox[name=file]:checked").each(function () {
            // foreach checked input, concat value
            result = result + $(this).val();
            // if a file was concatenated, add a space, else it stays ''
            if (result !== '')
                result = result + ' ';
        });
        $('#copy-input').empty();
        if (result !== '')
            result = 'git add ' + result;
        $('#copy-input').val(result);
        // if nothing was concatenated, result stays ''
    });

    $('.btn-copy').click(function () {
        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copying text command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to copy');
        }
    });

});

