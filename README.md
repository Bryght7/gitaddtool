# GitAddTool
A small GUI tool to quickly write git add commands.

Love using command-lines git and don't want to use a full git GUI ?
Found yourself wasting time writing your `git add` command by hand ? 

This little application reads your console's response to a `git status` command, and nicely shows the list of the files that can be added for commit. You can then choose which files you want to include and the application will generate the corresponding `git add` command.

![example 3](http://i.imgur.com/gCfFrx1.png)

## FEATURES
* Simple and fast interface
* Recognize files from any pasted content from your console
* Recognize and split _modified_ and _untracked_ files
* Copy the generated command with a button

## DOWNLOAD
* [__RELEASES__ page](https://gitlab.com/Bryght7/gitaddtool/tags).

## INSTALLATION

### Using binaries
* __WINDOWS__
    * Install by following the instructions of the setup executable.
* __LINUX__
    * Uncompress the archive to the install directory of your choice.
    * To directly launch GitAddTool without installing :
        * `chmod +x GitAddTool` if needed, then run `./GitAddTool`.
    * To install the desktop application :
        * Run `./install.sh`. You may have to use `sudo ./install.sh` if needed.
        * Follow the instructions.
        * To uninstall, run `./uninstall.sh`. You may have to use `sudo ./uninstall.sh` if needed.
* __MAC__
    * Binaries were not yet tested for Mac OS. Feedback is welcomed.

### Building from sources
* Building instructions coming soon.

## USAGE
1) Execute a `git status` command in your console / terminal.

2) Copy your console's response. You can copy from anywhere to anywhere but your selection has to include the file names.

![example 1](http://i.imgur.com/OgGtS5f.png)

3) Launch __GitAddTool__.

4) Paste your selection in the black area and click on the `Read` button.

![example 2](http://i.imgur.com/D05ehrM.png)

5) Check the files you wish to include in the `git add` command.

6) Click on the green button to copy the generated `git add` command.

7) You are done ! You can now paste the command in your console.

## LICENSE
```
MIT License

Copyright (c) 2017 Bryan Lee

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```